export const environment = {
  production: true,

  // RESUME IMAGES
  avatar: 'assets/image/cv/avatar.jpg',
  company: 'assets/image/cv/company.png',
  education: 'assets/image/cv/education.png',
  email: 'assets/image/cv/email.png',
  flilia: 'assets/image/cv/flilia.png',
  phone: 'assets/image/cv/phone.png',

  // FONT
  fonts: {
    roboto: 'http://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxPKTU1Kg.ttf',
  }
};
