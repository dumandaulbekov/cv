// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // RESUME IMAGES
  avatar: 'assets/image/cv/avatar.jpg',
  company: 'assets/image/cv/company.png',
  education: 'assets/image/cv/education.png',
  email: 'assets/image/cv/email.png',
  flilia: 'assets/image/cv/flilia.png',
  phone: 'assets/image/cv/phone.png',

  // FONT
  roboto: 'http://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxPKTU1Kg.ttf',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
