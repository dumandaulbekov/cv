import { Injectable } from '@angular/core';
import { summary } from '../shared/summary';
import { IResume, IContact, IJobPreference, IAbout, IWorkExperience, IEducation, ICertification, ILanguage, ISkill, IUser } from '../shared/interface';
import { environment } from 'src/environments/environment.prod';

const pdfMake = require('pdfmake/build/pdfmake.js');
pdfMake.fonts = {
  Roboto: {
    normal: environment.fonts.roboto,
    bold: environment.fonts.roboto
  }
}

@Injectable({
  providedIn: 'root'
})

export class PrintService {
  private docs: object;

  constructor() { }

  open() {
    setTimeout(async () => {
      const docs = await this.generateCV(summary);

      const pdfDocGenerator = pdfMake.createPdf(docs);
      pdfDocGenerator.getDataUrl((dataUrl) => {
        const iframe = document.createElement('iframe');
        const file = document.querySelector('#cv');

        iframe.src = dataUrl;
        iframe.style.width = '100%';
        iframe.style.height = '100%';

        file.appendChild(iframe);
      });
    }, 1000);
  }

  public async PDFAction(action: 'download' | 'open'): Promise<void> {
    this.docs = await this.generateCV(summary);

    switch (action) {
      case 'open': pdfMake.createPdf(this.docs).open({}, window); break;
      case 'download': pdfMake.createPdf(this.docs).download('Daulbekov Duman'); break;
    }
  }

  async convertImgToBase64(imageUrl): Promise<string | ArrayBuffer> {
    const res = await fetch(imageUrl);
    const blob = await res.blob();

    const promise = new Promise<string | ArrayBuffer>((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => resolve(reader.result);
      reader.onerror = () => reject(new Error('Error at converter image to base 64'));

      reader.readAsDataURL(blob);
    });

    return await promise;
  }

  private async generateCV(resume: IResume): Promise<object> {
    return {
      // METADATA
      info: {
        title: 'Title',
        author: 'author',
        subject: 'subject',
        keywords: 'keywords',
        creator: 'creator',
        producer: 'producer',
        creationDate: '01.01.2020.',
        modDate: '01.01.2020',
        trapped: 'trapped',
      },

      defaultStyle: {
        font: 'Roboto',
        fontSize: '10'
      },

      content: [
        this.generateUserInfoSection(resume.content.user, resume.content.primaryContact, resume.content.messengers),
        resume.content.jobPreferences ? this.generateJobPreferenceSection(resume.content.jobPreferences) : '',
        resume.content.about ? this.generateAboutSection(resume.content.about) : '',
        resume.content.workexperience ? this.experience(resume.content.workexperience, 'Work experience') : '',
        resume.content.education ? this.experience(resume.content.education, 'Education') : '',
        resume.content.certification ? this.generateCertificationSection(resume.content.certification) : '',
        resume.content.language ? this.generateLanguageSection(resume.content.language) : '',
        resume.content.skill ? this.generateSkillSection(resume.content.skill) : '',
        this.generateQR(resume.content.user)
      ],

      footer: (currentPage, pageCount) => {
        return {
          stack: [
            {
              image: 'fliliaLabel', width: 25,
              margin: [38.5, 8, 0, 0],
              color: 'black',
            },
            {
              text: `${currentPage.toString() + '/' + pageCount}`,
              margin: [0, -18.5, 38.5, 7],
              style: ['text_9'],
              color: '#7f8c8d',
              alignment: 'right'
            }
          ],
        };
      },

      styles: {
        spacer: { margin: [0, 28, 0, 0] },
        bold: { bold: true },
        link: { color: '#2980b9', },
        muted: { color: '#7a7a7a' },

        // FONT SIZE
        text_20: { fontSize: 20 },
        text_9: { fontSize: 9 },
      },

      images: {
        avatar: await this.convertImgToBase64(environment.avatar),
        company: await this.convertImgToBase64(environment.company),
        education: await this.convertImgToBase64(environment.education),
        email: await this.convertImgToBase64(environment.email),
        fliliaLabel: await this.convertImgToBase64(environment.flilia),
        phone: await this.convertImgToBase64(environment.phone),
      },
    };
  }

  // USER
  private generateUserInfoSection(user: IUser, contacts: IContact[], messengers: IContact[]): object {
    return {
      columns: [
        {
          width: '20%',
          stack: [
            { image: user.avatar, width: 80, height: 80 },
            {
              canvas: [{ type: 'rect', x: 0, y: 0, w: 82, h: 82, r: 5, lineColor: 'white', lineWidth: 2.5, }],
              absolutePosition: { y: 39, x: 39, },
            }
          ]
        },
        {
          width: '80%',
          stack: [
            { text: user.name, style: ['text_20', 'bold'], margin: [0, 0, 0, 2] },
            {
              margin: [0, 0, 0, 8],
              Labelle: ['muted'],
              text: [
                { text: `Born ${user.born}` },
                { text: `\t${user.location}` },
              ]
            },
            contacts ? this.generateContactsSection(contacts, 'contact', [0, 0, 0, 8]) : '',
            messengers ? this.generateContactsSection(messengers, 'messengers', [0, 0, 0, 0]) : ''
          ],
        },
      ],
    };
  }

  private generateContactsSection(contacts: IContact[], generate: 'contact' | 'messengers', margin: number[]): object {
    const numberFilledCells = 2;
    const layout = this.tableBorderConfig('noBorders');
    const widths = ['2%', '48%', 1, '2%', '48%'];
    const body = [];

    if (contacts.length) {
      for (let i = 0; i < contacts.length; i += numberFilledCells) {
        const contact = contacts.slice(i, i + numberFilledCells);

        const firstCell = contact[0];
        const secondCell = contact[1];
        const spacerBetweenCells = '';

        const row = [];

        if (firstCell) {
          if (generate === 'contact') {
            row.push(
              { image: firstCell.image, width: 12, },
              { text: firstCell.value, style: 'link', }
            );
          }

          if (generate === 'messengers') {
            row.push(
              {
                colSpan: 2,
                stack: [
                  { text: firstCell.name, style: 'muted' },
                  { text: firstCell.value, margin: [0, 0, 0, 0] },
                ]
              },
              spacerBetweenCells,
            );
          }
        }

        if (secondCell) {
          if (generate === 'contact') {
            row.push(
              spacerBetweenCells,
              { image: secondCell.image, width: 12, },
              { text: secondCell.value, style: 'link', });
          }

          if (generate === 'messengers') {
            row.push(
              spacerBetweenCells,
              {
                colSpan: 2,
                stack: [
                  { text: secondCell.name, style: 'muted' },
                  { text: secondCell.value, margin: [0, 0, 0, 0] },
                ]
              },
            );
          }
        }

        while (row.length !== widths.length) {
          row.push('');
        }

        body.push(row);
      }

      return {
        margin,
        stack: [
          this.generateTable(layout, widths, body)
        ]
      };
    }
  }

  // JOB PREFERENCE
  private generateJobPreferenceSection(jobPreference: IJobPreference): object {
    const layout = this.tableBorderConfig('noBorders');
    const widths = [60, '*'];
    const body = [];

    if (jobPreference.industry) {
      body.push([{ text: 'Industry', style: 'muted' }, jobPreference.industry]);
    }

    if (jobPreference.jobTitlesNames) {
      body.push([{ text: 'Job titles', style: 'muted' }, jobPreference.jobTitlesNames.join(' • ')]);
    }

    if (jobPreference.jobLocationsNames) {
      body.push([{ text: 'Locations', style: 'muted' }, jobPreference.jobLocationsNames.join(' • ')]);
    }

    if (jobPreference.jobTypesNames) {
      body.push([{ text: 'Job types', style: 'muted' }, jobPreference.jobTypesNames.join(' • ')]);
    }

    return {
      style: ['spacer'],
      stack: [
        this.generateHeader('Job Preference'),
        this.generateTable(layout, widths, body),
      ]
    };

  }

  // ABOUT
  private generateAboutSection(data: IAbout): object {
    if (data.content !== undefined) {
      return {
        style: ['spacer'],
        stack: [
          this.generateHeader('About'),
          { text: data.content, alignment: 'justify' },
        ]
      };
    }
  }

  private experience(experiences: IWorkExperience[] | IEducation[], generate: 'Work experience' | 'Education'): object {
    const layout = this.tableBorderConfig('lineSizeAndColor');
    const widths = [8.5, 10, '95%'];
    const body = [];
    const image = generate === 'Work experience' ? 'company' : 'education';

    if (experiences.length) {
      experiences.forEach((experience) => {
        const header = [
          {
            border: this.borderConfig('noBorder'),
            margin: [0, -1, 0, 0],
            stack: [
              { image, width: '25', colSpan: 2, },
              {
                canvas: [{ type: 'rect', x: 0, y: 0, w: 27, h: 27, r: 4, lineColor: 'white', lineWidth: 3 }],
                relativePosition: { y: -26, x: - 1 }
              }
            ]
          },
          this.borderConfig('noBorderAndText'),
        ];

        const content = [
          this.borderConfig('noBorderAndText'),
          { text: '', border: this.borderConfig('borderLeft') },
        ];

        if (generate === 'Work experience') {
          header.push({
            border: this.borderConfig('noBorder'),
            stack: [
              experience.jobTitle ? { text: experience.jobTitle, style: ['bold'] } : '',
              experience.companyName ? { text: experience.companyName, style: ['muted'] } : '',
            ]
          });

          content.push({
            border: this.borderConfig('noBorder'),
            margin: [0, 1.5, 0, 0],
            stack: [
              {
                style: ['text_9', 'muted'],
                text: [
                  experience.country ? { text: experience.country } : '',
                  experience.city ? { text: `, ${experience.city}` } : '',
                ]
              },
              {
                style: ['text_9', 'muted'],
                margin: experience.description ? [0, 0, 0, 0] : [0, 0, 0, 8],
                text: [
                  experience.startDate ? { text: experience.startDate } : '',
                  experience.endDate ? { text: ` — ${experience.endDate}` } : '',
                  experience.employmentType ? { text: ` • ${experience.employmentType}` } : '',
                ]
              },
              experience.description
                ? {
                  text: experience.description,
                  alignment: 'justify',
                  margin: experience.id !== this.getLastValueId(experiences) ? [0, 8, 0, 8] : [0, 8, 0, 0]
                } : '',
            ]
          });
        }

        if (generate === 'Education') {
          header.push({
            border: this.borderConfig('noBorder'),
            stack: [
              experience.educationalInstitution ? { text: experience.educationalInstitution, style: ['bold'] } : '',
              experience.fieldOfStudy ? { text: experience.fieldOfStudy, style: ['muted'] } : '',
            ]
          });

          content.push({
            border: this.borderConfig('noBorder'),
            margin: [0, 1.5, 0, 0],
            stack: [
              {
                style: ['text_9', 'muted'],
                text: experience.country ? { text: experience.country } : '',
              },
              {
                style: ['text_9', 'muted'],
                margin: experience.description ? [0, 0, 0, 0] : [0, 0, 0, 8],
                text: [
                  experience.graduationDate ? { text: `Graduated at ${experience.graduationDate}` } : '',
                  experience.degree ? { text: ` • ${experience.degree}` } : ''
                ],
              },
              experience.description
                ? {
                  text: experience.description,
                  alignment: 'justify',
                  margin: experience.id !== this.getLastValueId(experiences) ? [0, 8, 0, 8] : [0, 8, 0, 0]
                } : '',
            ]
          });
        }

        body.push(header, content);
      });

      return {
        style: ['spacer'],
        stack: [
          this.generateHeader(generate),
          this.generateTable(layout, widths, body),
        ]
      };
    }
  }

  // CERTIFICATION
  private generateCertificationSection(certifications: ICertification[]): object {
    const body = [];
    const layout = this.tableBorderConfig('lineSizeAndColor');
    const widths = [1, '*'];

    if (certifications.length) {
      certifications.forEach((certification) => {
        const content = [
          {
            colSpan: 2,
            margin: [5, 5],
            stack: [
              certification.name ? { text: certification.name, style: ['bold'] } : '',
              certification.issuingOrganization ? { text: certification.issuingOrganization, style: ['muted'] } : '',
              {
                style: ['text_9', 'muted'],
                margin: certification.issueDate ? [0, 7, 0, 0] : [0, 0, 0, 0],
                text: [
                  certification.issueDate ? { text: `Issued at ${certification.issueDate}` } : '',
                  certification.expirationDate ? { text: ` — Expire at ${certification.expirationDate}` } : '',
                ]
              },
              certification.description
                ? { text: certification.description, alignment: 'justify', margin: [0, 8, 0, 0] }
                : ''
            ]
          }
        ];

        if (certification.id !== this.getLastValueId(certifications)) {
          const spacer = [{ border: this.borderConfig('borderRight'), text: '' }];

          while (widths.length !== spacer.length) {
            spacer.push(
              {
                border: this.borderConfig('noBorder'),
                text: ''
              });
          }

          body.push(content, spacer);
        } else {
          body.push(content);
        }
      });

      return {
        style: ['spacer'],
        stack: [
          this.generateHeader('Certification'),
          this.generateTable(layout, widths, body),
        ]
      };
    }

  }

  // LANGUAGE
  private generateLanguageSection(languages: ILanguage[]): object {
    const cell = 3;
    const body = [];
    const widths = ['33.3%', '0.1%', '33.3%', '0.1%', '33.3%'];
    const layout = this.tableBorderConfig('noBorders');

    if (languages.length) {
      for (let i = 0; i < languages.length; i += cell) {
        const language = languages.slice(i, i + cell);

        const firstCell = language[0];
        const secondCell = language[1];
        const thirdCell = language[2];
        const spacerBetweenCells = '';

        const row = ([
          firstCell ? [
            { text: firstCell.language, style: 'bold' },
            { text: firstCell.languageProficiency, style: 'muted' },
          ] : '',
          spacerBetweenCells,
          secondCell ? [
            { text: secondCell.language, style: 'bold' },
            { text: secondCell.languageProficiency, style: 'muted' },
          ] : '',
          spacerBetweenCells,
          thirdCell ? [
            { text: thirdCell.language, style: 'bold' },
            { text: thirdCell.languageProficiency, style: 'muted' },
          ] : '',
        ]);

        language.forEach((v) => {
          if (v.id !== this.getLastValueId(languages)) {
            body.push(row, [{ colSpan: widths.length, text: '' }]);
          } else {
            body.push(row);
          }
        });
      }

      return {
        style: ['spacer'],
        stack: [
          this.generateHeader('Languages'),
          this.generateTable(layout, widths, body),
        ]
      };
    }
  }

  // SKILL
  private generateSkillSection(skills: ISkill[]): object {
    const skill = [];
    if (skills.length) {
      skills.forEach((c) => skill.push(c.title));

      return {
        style: ['spacer'],
        stack: [
          this.generateHeader('Skills and abbilities'),
          { text: skill.join('      •      ') }
        ]
      };
    }
  }

  private generateQR(user: IUser): object {
    return {
      alignment: 'center',
      margin: [0, 40, 0, 0],
      stack: [
        { qr: `https://flilia/me/${user.name}`, fit: '80', },
        { text: `https://flilia/me/${user.name.toLowerCase().replace(/\s/g, '')}`, margin: [0, 6, 0, 0] }
      ]
    };
  }

  // -----------------------------------------
  private getLastValueId(data): number {
    const value = data[data.length - 1];
    return value.id;
  }

  private generateHeader(name: string): object {
    return {
      text: name.toUpperCase(),
      color: 'black', style: ['bold'], margin: [0, 0, 0, 6]
    };
  }

  private generateTable(layout, widths: any[], body): object {
    return {
      layout,
      table: {
        widths,
        body
      }
    };
  }

  private borderConfig(value: 'noBorder' | 'borderRight' | 'borderLeft' | 'noBorderAndText'): any {
    switch (value) {
      case 'noBorder':
        return [false, false, false, false];
      case 'borderRight':
        return [false, false, true, false];
      case 'borderLeft':
        return [true, false, false, false];
      case 'noBorderAndText':
        return {
          text: '',
          border: [false, false, false, false]
        };
    }
  }

  private tableBorderConfig(value: 'noBorders' | 'lineSizeAndColor'): object | string {
    switch (value) {
      case 'noBorders':
        return 'noBorders';
      case 'lineSizeAndColor':
        return {
          hLineWidth: () => 0.7,
          vLineWidth: () => 0.7,
          hLineColor: () => '#E0E0E0',
          vLineColor: () => '#E0E0E0',
        };
    }
  }
}
