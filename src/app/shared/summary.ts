import { IResume } from './interface';

export const summary: IResume = {
    content: {
        user: {
            avatar: 'avatar',
            name: 'Duman Daulbekov',
            born: '8 March 2000',
            location: 'Kazahstan, Astana'
        },

        primaryContact: [
            { name: 'Primary email', image: 'email', value: 'dumandaulbekov@gmail.com' },
            { name: 'Primary number', image: 'phone', value: '+77070428514' },
        ],

        messengers: [
            { name: 'Telegram', value: 'https://t.me/dumandaulbekov' },
            { name: 'Skype', value: 'http://localhost:4202/me/08198386-7c8b-4e84-b01e-e5d84c40be1d/edit/contacts' },
        ],

        jobPreferences: {
            industry: 'Investment Banking',
            jobTitlesNames: ['Activist'],
            jobLocationsNames: ['Netherlands'],
            jobTypesNames: ['Contract', 'Full-time', 'Part-time', 'Remote'],
        },

        about: {
            content: `Twenty years from now you will be more disappointed by the things that you didn't do than by the ones you did do. So throw off the bowlines. Sail away from the safe harbor. Catch the trade winds in your sails. Explore. Dream. Discover.
            \nHere's to the crazy ones. The misfits. The rebels. The troublemakers. The round pegs in the square holes. The ones who see things differently. They're not fond of rules. And they have no respect for the status quo. You can quote them, disagree with them, glorify or vilify them. About the only thing you can't do is ignore them. Because they change things. They push the human race forward. And while some may see them as the crazy ones, we see genius. Because the people who are crazy enough to think they can change the world, are the ones who do.`
        },

        workexperience: [
            {
                id: 1,
                jobTitle: 'NET Developer',
                employmentType: 'Contract',
                companyName: 'Flilia LLP',
                country: 'Kazakhstan',
                city: 'Nur-Sultan',
                startDate: 'January 2017',
                endDate: 'June 2025',
                description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
            },
        ],

        education: [
            {
                id: 1,
                educationalInstitution: 'New York Academy of Art',
                fieldOfStudy: 'Design',
                degree: 'Candidate of sciences',
                graduationDate: '2019',
                country: 'United States',
                description: `In 1984, the New York Academy of Art (NYAA) relocated to Lafayette Street in the East Village and expanded its administration, faculty, and curriculum, with additional support from Pivar.[7] By 1986, the New York Times reported that the NYAA had grown to serve 40 full-time students, all on scholarship, along with 150 part-time students enrolled in fee-based night classes.[7] The arts curricula centered on building the classical skills of realism including training in perspective drawing and working from life models to sculpt the human form.

                David Kratz was appointed president of the school in 2009.[10][2] Prior to his leadership appointment at NYAA, Kratz had worked in public relations for two decades before he earned a graduate degree in painting from the academy in 2008.`
            },
        ],

        language: [
            { id: 2, language: 'Kazakh', languageProficiency: 'Advanced (C1)' },
        ],
    },
};
