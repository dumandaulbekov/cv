export interface IResume {
    content?: {
        user: IUser;
        primaryContact?: IContact[];
        messengers?: IContact[];
        about?: IAbout;
        jobPreferences?: IJobPreference;
        education?: IEducation[];
        workexperience?: IWorkExperience[];
        certification?: ICertification[];
        language?: ILanguage[];
        skill?: ISkill[];
    };
}

export interface IUser {
    name: string;
    born: string;
    avatar: string;
    location: string;
}

export interface IContact {
    name: string;
    image?: string;
    value: string;
}

export interface IJobPreference {
    industry?: string;
    jobTitlesNames?: string[];
    jobTypesNames?: string[];
    jobLocationsNames?: string[];
    openForOffers?: boolean;
}

export interface IAbout {
    content?: string;
}

export interface IWorkExperience {
    id: number;
    jobTitle?: string;
    employmentTypeValue?: string;
    employmentType?: string;
    companyName?: string;
    countryValue?: string;
    country?: string;
    city?: string;
    isCurrentWork?: boolean;
    startDate?: string;
    endDate?: string;
    description?: string;
}

export interface IEducation {
    id: number;
    educationalInstitution?: string;
    fieldOfStudy?: string;
    degree?: string;
    country?: string;
    graduationDate?: string;
    description?: string;
}

export interface ICertification {
    id: number;
    name?: string;
    issuingOrganization?: string;
    issueDate?: string;
    expirationDate?: string;
    isNotExpire?: boolean;
    description?: string;
}

export interface ILanguage {
    id: number;
    language: string;
    languageProficiency?: string;
}

export interface ISkill {
    id: number;
    title: string;
}
