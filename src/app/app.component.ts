import { Component, OnInit } from '@angular/core';
import { PrintService } from './service/print.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private printService: PrintService,
  ) { }

  ngOnInit(): void {
    this.printService.open();
  }

  public print(action: 'download' | 'open'): void {
    this.printService.PDFAction(action);
  }
}
